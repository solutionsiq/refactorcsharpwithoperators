﻿namespace RefactorCSharpWithOperators
{
    public class PersonService
    {
        public string GetZipCode(Person person)
        {
            //return GetZipCodeVersion1(person);
            return GetZipCodeWithNullConditionalOperator(person);
        }

        private static string GetZipCodeVersion1(Person person)
        {
            if (person != null)
            {
                if (person.Address != null)
                {
                    return person.Address.Zip;
                }
            }

            return null;
        }

        /// <summary>
        /// Refactored GetZipCodeVersion1 to sue the Null-conditional Operator.
        /// https://msdn.microsoft.com/en-us/library/dn986595.aspx
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        private static string GetZipCodeWithNullConditionalOperator(Person person)
        {
            return person?.Address?.Zip;
        }
    }
}