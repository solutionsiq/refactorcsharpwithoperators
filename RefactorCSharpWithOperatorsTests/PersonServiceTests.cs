﻿using NUnit.Framework;
using RefactorCSharpWithOperators;

namespace RefactorCSharpWithOperatorsTests
{
    [TestFixture]
    public class PersonServiceTests
    {
        [Test]
        public void ZipCode()
        {
            //Arrange
            var personService = new PersonService();
            var personWithZip = new Person
            {
                Name = "tester",
                Age = 18,
                Address = new Address
                {
                    Zip = "98052"
                }
            };

            //Act
            var actual = personService.GetZipCode(personWithZip);

            //Assert
            Assert.That(actual, Is.EqualTo("98052"));
        }


        [Test]
        public void ZipCodeWherePersonIsNull()
        {
            //Arrange
            var personService = new PersonService();
            Person nullPerson = null;

            //Act
            var actual = personService.GetZipCode(nullPerson);

            //Assert
            Assert.That(actual, Is.Null);
        }


        [Test]
        public void ZipCodeWhereAddressIsNull()
        {
            //Arrange
            var personService = new PersonService();
            var personWithoutAddress = new Person
            {
                Name = "tester",
                Age = 18,
                Address = null
            };

            //Act
            var actual = personService.GetZipCode(personWithoutAddress);

            //Assert
            Assert.That(actual, Is.Null);
        }

        [Test]
        public void ZipCodeWhereZipCodeIsNull()
        {
            //Arrange
            var personService = new PersonService();
            var person = new Person
            {
                Name = "tester",
                Age = 18,
                Address = new Address
                {
                    Zip = null
                }
            };

            //Act
            var actual = personService.GetZipCode(person);

            //Assert
            Assert.That(actual, Is.Null);
        }
    }
}