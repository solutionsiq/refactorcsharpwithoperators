# README #

Examples of how to refactor code with units tests (NUnit) using operators in C#.

### Refactoring ###

Refactoring is the act of changing the implementation of source code without chaning the expected result.
This can include simple varable name changes, removal of comments/dead-code, and even architechtual shifts.
It's important to have unit tests covering all lines of affected source code so that you have proof your 
refactorings didn't change the functionality.

### Null-conditional Operator ###

The unit tests in PersonServiceTests ensure the source code works for the happy path case and several null cases.
The PersonService.GetZipCode() method first uses basic conditional statements to check for nulls.  For this example,
I left the methos GetZipCodeVersion1() and GetZipCodeWithNullConditionalOperator() in place to show the differences.
